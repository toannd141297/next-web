const Hero = () => {
  return (
    <section className="bg-pattern-circuit">
      <div className="container">
        <div className="content ">
          <h1>Hi, my name is Toan Nadi.</h1>
          <h2>
            I design pretty things with <span>Figma</span> and build them with
            <span>TypeScript</span> & <span>React</span>
          </h2>
        </div>
      </div>
    </section>
  );
};

export default Hero;
