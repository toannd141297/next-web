import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router";
import { RiMenuFoldFill } from "react-icons/ri";
import userData from "../../constants/data"

const Header = () => {
  const router = useRouter();

  const [state, setstate] = useState(false);
  const handelShow = () => {
    setstate(!state);
  };
  return (
    <header>
      <div className="position-fix">
        <div className="header">
          <Link href="/">
            <a className="header-logo">Toannadi</a>
          </Link>
          <button className="btn btn--close" onClick={() => handelShow()}>
            <RiMenuFoldFill fill="#fff" fontSize="2.25rem" />
          </button>
          <ul className="desktop">
            <li>
              <Link href="/">
                <a className={router.pathname == "/" ? "active" : ""}>Home</a>
              </Link>
            </li>
            <li>
              <Link href="/about">
                <a className={router.pathname == "/about" ? "active" : ""}>
                  About
                </a>
              </Link>
            </li>
            <li>
              <Link href="/portfolio">
                <a className={router.pathname == "/portfolio" ? "active" : ""}>
                  Portfolio
                </a>
              </Link>
            </li>
            <li>
              <Link href="/blog">
                <a className={router.pathname == "/blog" ? "active" : ""}>
                  Blog
                </a>
              </Link>
            </li>
          </ul>
          <ul className={state ? "mobile active" : "mobile "}>
            <li>
              <Link href="/">
                <a className={router.pathname == "/" ? "active" : ""}>Home</a>
              </Link>
            </li>
            <li>
              <Link href="/about">
                <a className={router.pathname == "/about" ? "active" : ""}>
                  About
                </a>
              </Link>
            </li>
            <li>
              <Link href="/portfolio">
                <a className={router.pathname == "/portfolio" ? "active" : ""}>
                  Portfolio
                </a>
              </Link>
            </li>
            <li>
              <Link href="/blog">
                <a className={router.pathname == "/blog" ? "active" : ""}>
                  Blog
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default Header;
