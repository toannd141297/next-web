import React, { useState, useEffect, useRef, useCallback } from "react";
import TimeSlider from "react-input-slider";
import { useSelector } from "react-redux";

import VolumeSlider from "react-input-slider";
import {
  ImPlay3,
  ImPause2,
  ImBackward2,
  ImForward3,
  ImVolumeMute2,
  ImVolumeHigh,
} from "react-icons/im";

const Player = () => {
  const audios = useSelector((state) => state.audios.audiosData);

  const audioRef = useRef();
  const [audioIndex, setAudioIndex] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [volume, setVolume] = useState(true);
  const [volumeCurrent, setVolumeCurrent] = useState(30);
  const [volumeMax, setVolumeMax] = useState(100);
  const [isPlay, setPlay] = useState(false);

  // console.log(audioIndex, "audioIndex");

  const handleLoadedData = () => {
    setDuration(audioRef.current.duration);
    if (isPlay) audioRef.current.play();
  };

  const handlePausePlayClick = () => {
    if (isPlay) {
      audioRef.current.pause();
    } else {
      audioRef.current.play();
    }
    setPlay(!isPlay);
  };

  const handleTimeSliderChange = ({ x }) => {
    audioRef.current.currentTime = x;
    setCurrentTime(x);

    if (!isPlay) {
      setPlay(true);
      audioRef.current.play();
    }
  };
  const handleVolume = ({ x }) => {
    let vol = x / 100;
    if (isNaN(vol)) {
      vol = 0.5;
    }
    audioRef.current.volume = parseFloat(vol);
    setVolumeCurrent(x);
    if (x === 0) {
      setVolume(false);
    } else setVolume(true);
  };
  const muteVolume = () => {
    setVolume(!volume);
    if (volume) {
      audioRef.current.volume = 0;
    } else audioRef.current.volume = parseFloat(volumeCurrent / 100);
  };
  const autoPlay = () => {
    audioRef.current.play();
  };
  const updatePlay = useCallback(() => {
    console.log(audioIndex);
    autoPlay();
  }, [audioIndex]);

  useEffect(() => {
    if (currentTime === duration) {
      setAudioIndex((audioIndex + 1) % audios.length);
    }
  }, [currentTime]);
  const backPlay = () => {
    if (audioIndex > 0) {
      setAudioIndex((audioIndex - 1) % audios.length);
    } else setAudioIndex(audios.length - 1);
  };
  return (
    <div className="playControls">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-6">
            <div className="control-audio">
              <div className="volume">
                <div className="volume-control">
                  <VolumeSlider
                    className="volume-slider"
                    axis="x"
                    xmax={volumeMax}
                    x={volumeCurrent}
                    onChange={handleVolume}
                    styles={{
                      track: {
                        backgroundColor: "#fff",
                        height: "5px",
                        cursor: "pointer",
                      },
                      active: {
                        backgroundColor: "#20bdff ",
                        height: "5px",
                      },
                      thumb: {
                        width: "10px",
                        height: "10px",
                        backgroundColor: "#20bdff",
                        borderRadius: 0,
                        cursor: "pointer",
                      },
                    }}
                  />
                </div>
                <div className="btn volume-btn" onClick={() => muteVolume()}>
                  {volume ? (
                    <ImVolumeHigh fontSize="1.5rem" />
                  ) : (
                    <ImVolumeMute2 fontSize="1.5rem" />
                  )}
                </div>
              </div>

              <TimeSlider
                className="time"
                axis="x"
                xmax={duration}
                x={currentTime}
                onChange={handleTimeSliderChange}
                styles={{
                  track: {
                    backgroundColor: "#e3e3e3",
                    height: "5px",
                    cursor: "pointer",
                  },
                  active: {
                    backgroundColor: "#20bdff ",
                    height: "5px",
                  },
                  thumb: {
                    width: "10px",
                    height: "10px",
                    backgroundColor: "#20bdff",
                    borderRadius: 0,
                    cursor: "pointer",
                  },
                }}
              />

              <div className="control-button">
                <div className="btn" onClick={() => backPlay()}>
                  <ImBackward2 fontSize="1.5rem" />
                </div>
                <div className="btn" onClick={handlePausePlayClick}>
                  {isPlay ? (
                    <ImPause2 fontSize="1.5rem" />
                  ) : (
                    <ImPlay3 fontSize="1.5rem" />
                  )}
                </div>
                <div
                  className="btn"
                  onClick={() =>
                    setAudioIndex((audioIndex + 1) % audios.length)
                  }
                >
                  <ImForward3 fontSize="1.5rem" />
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4 col-6">
            <div className="name-audio">
              <h2 className="song-title">{audios[audioIndex].title}</h2>
              <p className="singer">{audios[audioIndex].artist}</p>
            </div>
          </div>
        </div>

        <audio
          ref={audioRef}
          src={audios[audioIndex].src}
          onLoadedData={handleLoadedData}
          onTimeUpdate={() => setCurrentTime(audioRef.current.currentTime)}
          onEnded={() => setPlay(false)}
          // onVolumeChange={(event) => console.log(event,"xxx")}
        />
      </div>
    </div>
  );
};

export default Player;
