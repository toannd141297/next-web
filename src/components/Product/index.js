import Link from "next/link";

const Product = () => {
  return (
    <section className="top-product">
      <div className="container">
        <h3 className="top-product__title">Projects</h3>
        <div className="row">
          <div className="col-md-3">
            <div className="top-product__img">
              <img src="/images/1.png" alt="Bitcastle" />
            </div>
          </div>
          <div className="col-md-9">
            <p className="top-product__content">
              <span>Lorem Ipsum </span>is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged.
            </p>
            <div className="top-product__link"> 
              <Link href="">Figma</Link>
              <Link href="">Website</Link>
            </div>
          </div>
        </div>
        <div className="text-center">
        <button  className="btn btn--showMore"> Show more </button>
        </div>
      </div>
    </section>
  );
};

export default Product;
