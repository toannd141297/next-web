import Modal from "react-awesome-modal";
import { useDispatch, useSelector } from "react-redux";
import PopupTypes from "../../redux/popup-redux";
import { BiX, BiLinkAlt } from "react-icons/bi";
import { FiFigma } from "react-icons/fi";

import Link from "next/link";

export default function PopupDetail({ item }) {
  const dispatch = useDispatch();
  const popup = useSelector((state) => state.popup.showDetailPopup);
  const dataPopup = useSelector((state) => state.popup.dataPopup.item);
  const closeModal = () => {
    dispatch(
      PopupTypes.closeDetailPopup({
        popupName: "showDetailPopup",
      })
    );
  };

  return (
    <div className="modal-xl ">
      <Modal
        visible={popup}
        width="100%"
        height="90%"
        effect="fadeInUp"
        onClickAway={() => closeModal()}
        className="modal-xl"
      >
        <div className="modal-body">
          <div className="container">
            <button className="btn btn--close" onClick={() => closeModal()}>
              <BiX fontSize={40} />
            </button>
            <h1 className="mt-4">{dataPopup.title}</h1>
            <p> {dataPopup.desc}</p>
            <ul className="action">
              <li>
                <Link href="https://figma.com">
                  <a target="_blank">
                    <i>
                      <FiFigma fontSize={24} />
                    </i>
                    Tool
                  </a>
                </Link>
              </li>
              <li>
                <Link href={dataPopup.link}>
                  <a target="_blank">
                    <i>
                      <BiLinkAlt fontSize={24} />
                    </i>
                    View more
                  </a>
                </Link>
              </li>
            </ul>

            {dataPopup.imgUrl.map((item, index) => {
              return (
                <div className="img" key={index}>
                  <img src={item} />
                </div>
              );
            })}
          </div>
        </div>
      </Modal>
    </div>
  );
}
