import Head from "next/head";
import { useRouter } from "next/router";

export default function SEO({ ...props }) {
  const router = useRouter();

  const meta = {
    title: "Toannadi - Designer, Developer and Creator",
    description: `I've been developing websites for 5 years straight. Get in touch with me to know more.`,
    image: "avatar.png",
    type: "website",
    keywords: "Toannadi, Developer, Designer, Creator, Toàn Nadi",
    ...props,
  };
  return (
    <Head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta
        http-equiv="Content-Security-Policy"
        content="upgrade-insecure-requests"
      />
      <title>{meta.title}</title>
      <meta name="robots" content="follow, index" />
      <meta content={meta.description} name="description" />
      <meta property="og:url" content={`https://www.betterlife.asia`} />
      <link
        rel="canonical"
        href={`https://www.betterlife.asia${router.asPath}`}
      />
      <meta property="og:type" content={meta.type} />
      <meta property="og:site_name" content="Toan Nadi" />
      <meta property="og:description" content={meta.description} />
      <meta property="og:keywords" content={meta.keywords} />
      <meta property="og:title" content={meta.title} />
      <meta property="og:image" content={meta.image} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@toannadi" />
      <meta name="twitter:title" content={meta.title} />
      <meta name="twitter:description" content={meta.description} />
      <meta name="twitter:image" content={meta.image} />
      {meta.date && (
        <meta property="article:published_time" content={meta.date} />
      )}
    </Head>
  );
}
