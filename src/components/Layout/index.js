import Head from "next/head";
import dynamic from "next/dynamic";
import { Fragment } from "react";

const Header = dynamic(() => import("../Header"));
function Layout({ ...props }) {
  return (
    <Fragment>
      <Header/>
      {props?.children}
      {/* <Player /> */}
    </Fragment>
  );
}

export default Layout;
