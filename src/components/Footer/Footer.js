import styles from "./Footer.module.scss";
import { FcAbout } from "react-icons/fc";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <a
        href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img src="/vercel.svg" alt="Vercel Logo" className="footer_img" />
      </a>
      <div>
        <FcAbout />
      </div>
    </footer>
  );
};

export default Footer;
