import { combineReducers } from "redux";

const rootReducer = combineReducers({
  user: require("./user-redux").reducer,
  popup: require("./popup-redux").reducer,
  project: require("./project-redux").reducer,
  audios: require("./audio-redux").reducer,
  post: require("./post-redux").reducer,
});

export default rootReducer;
