import { createReducer, createActions } from "reduxsauce";
import audio from "../constants/audios";

const { Types, Creators } = createActions({
  projectRequest: [""],
});

export const ProjectTypes = Types;

export const INITIAL_STATE = {
  audiosData: audio,
};

export const reducer = createReducer(INITIAL_STATE, {
});

export default Creators;
