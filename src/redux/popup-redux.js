import { createActions, createReducer } from "reduxsauce";


const { Types, Creators } = createActions({
  openDetailPopup: ["dataPopup"],
  closeDetailPopup: ["dataPopup"],
});

export const PopupTypes = Types;

export const INITIAL_STATE = {
  dataPopup: {} ,
  showDetailPopup: false,
};

export const openPopup = (state, { dataPopup }) => {
  return {
    ...state,
    dataPopup,
    [dataPopup.popupName]: true
  };
};

export const closePopup = (state, { dataPopup }) => {
  return {
    ...state,
    dataPopup: {},
    [dataPopup.popupName]: false
  };
};

//TODO:Hookup Reducers To Types in Action
export const reducer = createReducer(INITIAL_STATE, {
  [PopupTypes.OPEN_DETAIL_POPUP]: openPopup,
  [PopupTypes.CLOSE_DETAIL_POPUP]: closePopup,
});

export default Creators;
