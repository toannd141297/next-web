import { createReducer, createActions } from "reduxsauce";
import userData from "../constants/data";

const { Types, Creators } = createActions({
  userRequest: [""],
});

export const UserTypes = Types;

export const INITIAL_STATE = {
  userData: userData,
};

export const reducer = createReducer(INITIAL_STATE, {
});

export default Creators;
