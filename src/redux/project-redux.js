import { createReducer, createActions } from "reduxsauce";
import project from "../constants/project";

const { Types, Creators } = createActions({
  projectRequest: [""],
});

export const ProjectTypes = Types;

export const INITIAL_STATE = {
  projectData: project,
};

export const reducer = createReducer(INITIAL_STATE, {
});

export default Creators;
