import { put, call } from "redux-saga/effects";
// import { getList } from "../services/apiMap.ts";
import postActions from "../redux/post-redux";
// import axios from "axios"
import { get } from "../services/api";

const postSagas = {
  *getPost(action) {
    try {
      const listPost = yield call(() => {
        return get("posts");
      });
      // const listPost = yield call(() => {
      //   return getList();
      // });
      console.log(listPost);
      if (listPost.status === 200) {
        yield put(
          postActions.getPostSucceed({
            listPost: listPost.data.message,
            errorMessage: "Succeed!",
          })
        );
      } else {
        yield put(postActions.postFailed("Lỗi mẹ rồi"));
      }
    } catch (error) {
      yield put(postActions.postFailed(error));
    }
  },
};

export default postSagas;
