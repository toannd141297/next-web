const audios = [
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Con+mua+thang+5+-+Buc+Tuong.mp3",
    title: "Cơn mưa tháng 5 - Bức Tường",
    artist: "Đất Việt",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Con+Duong+Khong+Ten+-+Buc+Tuong.mp3",
    title: "Con Đường Không Tên - Bức Tường",
    artist: "Con Đường Không Tên",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Bong+Hong+Thuy+Tinh+-+Buc+Tuong.mp3",
    title: "Bông Hồng Thủy Tinh - Bức Tường",
    artist: "Tâm Hồn Của Đá (The Soul Of Rock)",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Mat+Den+-+Buc+Tuong.mp3",
    title: "Mắt Đen - Bức Tường",
    artist: "Vô Hình",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Hoa+Ban+Trang+-+Buc+Tuong.mp3",
    title: "Hoa Ban Trắng- Bức Tường",
    artist: "Ngày Khác",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Cha+Va+Con+-+Buc+Tuong.mp3",
    title: "Cha và Con - Bức Tường",
    artist: "Nam Châm",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Tro+Ve+-+Buc+Tuong.mp3",
    title: "Trở Về - Bức Tường",
    artist: "Nam Châm",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Bo-Em-vao-Balo.mp3",
    title: "Bỏ Em Vào Balo",
    artist: "THE BEST OF ORINN MUSIC",
  },
  {
    src: "https://toannadi.s3.ap-southeast-1.amazonaws.com/audio/Thuc-Giac.mp3",
    title: "Thức Giấc - Da LAB",
    artist: "Thức Giấc (Single)",
  },
];
export default audios;
