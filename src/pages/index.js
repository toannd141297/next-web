import dynamic from "next/dynamic";
const Layout = dynamic(() => import("../components/Layout"));
const Hero = dynamic(() => import("../components/Hero"));
const Player = dynamic(() => import("../components/Card"));
const Seo = dynamic(() => import("../components/Seo"));
// import useSWR from "swr";

// const fetcher = (url) => fetch(url).then((res) => res.json());
export default function Home() {
  // const { data,error } = useSWR("/api/audios", fetcher);
  // if (error) return <div>Failed to load users</div>;
  // if (!data) return <div>Loading...</div>;
  return (
    <Layout>
      <Seo
        description="Hi, my name is Toan Nadi. I design pretty things with Figma and build them with TypeScript React"
        keywords="Toannadi, Design UI/UX, Front-End, Developer, Designer, Creator, Toàn Nadi"
      />
      <Hero />
    </Layout>
  );
}
