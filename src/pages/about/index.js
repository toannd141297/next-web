import dynamic from "next/dynamic";
import Link from "next/link";
import { useSelector } from "react-redux";
import { BiLinkAlt,BiArrowToBottom } from "react-icons/bi";
import { FiFigma, FiFacebook } from "react-icons/fi";

const Layout = dynamic(() => import("../../components/Layout"));
const Seo = dynamic(() => import("../../components/Seo"));

export default function About() {
  const userData = useSelector((state) => state.user.userData);
  return (
    <Layout>
      <Seo
        description=" I've worked with a range a technologies in the web development
        world. From Design UI/UX To Front-End
       "
        keywords="Design UI/UX, Front-End"
      />
      <section className="bg-pattern-circuit">
        <div className="container">
          <div className="content">
            <h2>Hey, I'am Toannadi</h2>
            <p className="about__content">
              I’m a <strong>designer</strong>, <strong>developer</strong> and
              <strong> creator</strong> . I work at Hanoi as a Ui/Ux Designer
              who is enthusiastic about the development of the world of
              information technology and also application development.
            </p>
            <p className="about__content">
              Have experience <strong>Design UI/UX</strong> using several web
              frameworks such as Bootstrap as a <strong>Front-End</strong> UI
              framework.
              <br></br>
              With more than 2 years of experience working in the field of UI /
              UX, i bring customers quality and reliable products.
              <br></br>
              After more than 4 years working with many different large and
              small partners. I have gained valuable work experience and want
              the new product to be <strong>better</strong> ,
              <strong>faster</strong> to catch up with the{" "}
              <strong>future</strong>.
            </p>
            <button className="btn btn--download">
              <Link href={userData.resumeUrl}>
                <a target="_blank">
                  <BiArrowToBottom fontSize={20}/>
                  Download CV</a>
              </Link>
            </button>
          </div>
        </div>
      </section>
      <div className="container">
        <div className="about">
          <h2 className="title-about">Experience</h2>

          <div className="row mrb-5">
            <div className="col-md-7">
              {userData.experience.map((item, index) => {
                return (
                  <div className="experience" key={index}>
                    <div className="experience-item">
                      <p className="experience-date">{item.year}</p>
                      <p className="experience-company">
                        {item.company}{" "}
                        <Link href={item.companyLink}>
                          <a target="_blank">
                            <BiLinkAlt />
                          </a>
                        </Link>
                      </p>
                      <p className="experience-title">{item.title}</p>
                      <div className="experience-desc">
                        {item.desc}
                        <br />
                        {item.desc2}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>

            <div className="col-md-5">
              {userData.education.map((item, index) => {
                return (
                  <div className="experience" key={index}>
                    <div className="experience-item">
                      <p className="experience-date">{item.year}</p>
                      <p className="experience-company">
                        {item.company}
                        <Link href={item.companyLink}>
                          <a target="_blank">
                            <BiLinkAlt />
                          </a>
                        </Link>
                      </p>
                      <p className="experience-title">{item.title}</p>
                      <div className="experience-desc">{item.desc}</div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>

          <h2 className="title-about">Service</h2>
          <div className="row mrb-5">
            {userData.skill.map((item, index) => {
              return (
                <div className="col-md-4" key={index}>
                  <div className="service">
                    <ul>
                      <li className="d-flex justify-content-center">
                        <i>{item.icon}</i>
                      </li>
                      <li>
                        <h5 className="service__title">{item.title}</h5>
                      </li>
                    </ul>
                  </div>
                </div>
              );
            })}
          </div>

          <h2 className="title-about">Tool</h2>
          <div className="row mrb-5">
            {userData.tools.map((item, index) => {
              return (
                <div className="col-md-3" key={index}>
                  <Link href={item.link}>
                    <a className="tool" target="_blank">
                      <div className="img">
                        <img src={item.imgUrl} />
                      </div>
                      <div className="fluid-w">
                        <p className="mb-2">{item.title}</p>
                        <p className="desc">{item.desc}</p>
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })}
          </div>
          <h2 className="title-about">Contact</h2>
          <div>
            <div className="row mrb-5">
              <div className="col-md-3">
                <div>
                  <p className="fs-18 mb-2 text2"> Address</p>
                  <p className="text1">{userData.address}</p>
                </div>
              </div>
              <div className="col-md-3">
                <div>
                  <p className="fs-18 mb-2 text2"> Phone</p>
                  <Link href={`tel:${userData.phone}`}>
                    <a className="text1">{userData.phone}</a>
                  </Link>
                </div>
              </div>
              <div className="col-md-3">
                <div>
                  <p className="fs-18 mb-2 text2"> Email</p>
                  <Link href={`mailto:${userData.email}`}>
                    <a className="text1">{userData.email}</a>
                  </Link>
                </div>
              </div>
              <div className="col-md-3">
                <div className="communication">
                  <Link href={userData.figma}>
                    <a target="_blank">
                      <i>
                        <FiFigma fontSize={24} />
                      </i>
                    </a>
                  </Link>
                  <Link href={userData.facebook}>
                    <a target="_blank">
                      <i>
                        <FiFacebook fontSize={24} />
                      </i>
                    </a>
                  </Link>
                  <Link href={userData.ui4free}>
                    <a target="_blank">
                      <i>
                        <BiLinkAlt fontSize={24} />
                      </i>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
