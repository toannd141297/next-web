import dynamic from "next/dynamic";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import PostTypes from "../../redux/post-redux";

const Layout = dynamic(() => import("../../components/Layout"));
const Seo = dynamic(() => import("../../components/Seo"));

export default function Blog() {
  const dispatch = useDispatch();
  const listPost = useSelector((state) => state.post.listPost);
  // console.log(listPost)

  useEffect(() => {
    dispatch(PostTypes.postRequest());
  }, []);
  return (
    <Layout>
      <Seo
        siteTitle="Blog"
        description=" I've worked with a range a technologies in the web development
        world. From Design UI/UX To Front-End
       "
        keywords="Design UI/UX, Front-End, Blog"
      />
      <section className="bg-pattern-circuit">
        <div className="container">
          <div className="content">
            <h2>Blog</h2>
            <p className="content-h2">
              <strong>Projects</strong> , <strong>experiment</strong> , and{" "}
              <strong>discoveries</strong>
            </p>
          </div>
        </div>
      </section>
      <div className="container">
        <h3>Hey, you're early!</h3>
        <p>I don't know what to do.</p>
        <p>Listen to music while you wait what i do with it</p>
        <ul>
          {listPost.map((item, index) => {
            return <li key={index}>{item.title}</li>;
          })}
        </ul>
      </div>
    </Layout>
  );
}
