import dynamic from "next/dynamic";
const Layout = dynamic(() => import("../../components/Layout"));
const Hero = dynamic(() => import("../../components/Hero"));
const Player = dynamic(() => import("../../components/Card"));
const Seo = dynamic(() => import("../../components/Seo"));
import useSWR from "swr";

const fetcher = (url) => fetch(url).then((res) => res.json());
export default function Home() {
  const { data, error } = useSWR("/api/audios", fetcher);
  if (error)
    return (
      <Layout>
        <div className="container">
          <div className="about">
            <h1 className="">error</h1>
          </div>
        </div>
      </Layout>
    );
  if (!data) return <div>Loading...</div>;
  console.log(data);
  return (
    <Layout>
      <div className="container">
        <div className="about">
          <h1 className="">test</h1>

          {data.map((item, index) => {
            return <div key={index}>{item.title}</div>;
          })}
        </div>
      </div>
    </Layout>
  );
}
