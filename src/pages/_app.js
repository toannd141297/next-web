import "../styles/globals.scss";

import dynamic from "next/dynamic";
import { Provider } from "react-redux";
import configureStore from "../redux/index";

const Player = dynamic(() => import("../components/Card"));

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={configureStore()}>
      <Component {...pageProps} />
      <Player />
    </Provider>
  );
}

export default MyApp;
