import dynamic from "next/dynamic";
import { useDispatch, useSelector } from "react-redux";
import PopupDetail from "../../components/PopupDetail";
import PopupTypes from "../../redux/popup-redux";

const Layout = dynamic(() => import("../../components/Layout"));
const Seo = dynamic(() => import("../../components/Seo"));

export default function Portfolio() {
  const dispatch = useDispatch();
  const ProjectData = useSelector((state) => state.project.projectData);
  const popup = useSelector((state) => state.popup.showDetailPopup);

  const openModal = (item) => {
    dispatch(
      PopupTypes.openDetailPopup({
        item,
        popupName: "showDetailPopup",
      })
    );
  };

  return (
    <Layout>
      <Seo
        description="Projects, experimentations, and discoveries"
        keywords="Design UI/UX, Front-End, Projects, experimentations, discoveries"
      />

      <section className="bg-pattern-circuit">
        <div className="container">
          <div className="content">
            <h2>Toannadi's Portfolio</h2>
            <p className="content-h2">
              <strong>Projects</strong> , <strong>experiment</strong> , and
              <strong> discoveries</strong>
            </p>
          </div>
        </div>
      </section>
      <div className="container pdb-5">
        <div className="row">
          {ProjectData.map((item, index) => {
            return (
              <div className="col-md-4" key={index}>
                <div className="portfolio" onClick={() => openModal(item)}>
                  <div className="portfolio_img">
                    <img src={item.thumbnail} alt={item.title} />
                  </div>
                  <div className="portfolio_title">
                    <h2>{item.title}</h2>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <h3>Hey, you're early!</h3>
        <p>You can find my old portfolio here.</p>
        <ul>
          <li>
            <a href="https://ui4free.com/user/toannadi" target="_blank">
              https://ui4free.com/user/toannadi
            </a>
          </li>
          <li>
            <a href="https://www.figma.com/@toannadi" target="_blank">
              https://www.figma.com/@toannadi{" "}
            </a>
          </li>
        </ul>
      </div>
      {popup && <PopupDetail />}
    </Layout>
  );
}
