import dynamic from "next/dynamic";
const Layout = dynamic(() => import("../components/Layout"));
const Hero = dynamic(() => import("../components/Hero"));
const Player = dynamic(() => import("../components/Card"));
const Seo = dynamic(() => import("../components/Seo"));
// import useSWR from "swr";

// const fetcher = (url) => fetch(url).then((res) => res.json());
export default function Home() {
  // const { data,error } = useSWR("/api/audios", fetcher);
  // if (error) return <div>Failed to load users</div>;
  // if (!data) return <div>Loading...</div>;
  return (
    <Layout>
      <section className="bg-pattern-circuit">
        <div className="container">
          <div className="content">
            <h2>Page Not Found 404 </h2>
          </div>
        </div>
      </section>
    </Layout>
  );
}
